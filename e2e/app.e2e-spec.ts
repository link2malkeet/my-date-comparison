import { DateDiffPage } from './app.po';

describe('date-diff App', () => {
  let page: DateDiffPage;

  beforeEach(() => {
    page = new DateDiffPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Date Comparison');
  });
});
