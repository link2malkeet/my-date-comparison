import { Injectable } from '@angular/core';
import { DateType } from './DateTypes';

@Injectable()
export class DateUtilityService {
  constructor() { }

  static noOfDaysInMonthMap: { [key: string]: number } = {
    "1": 31,
    "2": 28,
    "2L": 29,
    "3": 31,
    "4": 30,
    "5": 31,
    "6": 30,
    "7": 31,
    "8": 31,
    "9": 30,
    "10": 31,
    "11": 30,
    "12": 31

  }

  /**
   * find if a year is a leap year or not.
   * @param year 
   */
  isLeapYear(year: number) {
    if (year % 100 == 0) {
      if (year % 400 == 0) {
        return true;
      }
    } else if (year % 4 == 0) {
      return true;
    }
    return false;
  }

  /**
   * validate the dates on following parameters:
   * a) days must be in the range of 1 to 31.
   * b) months in the range of 1 to 12.
   * c) years in the range of 1900 to 2010.
   * @param date 
   */
  isAValidDate(date: DateType) {
    if (date.day < 0 || date.day > 31 || date.month < 0 || date.month > 12 || date.year < 1900
      || date.year > 2010) {
      return false;
    }
    let monthKey = date.month + "";
    if (date.month === 2) { // febrary
      monthKey = this.isLeapYear(date.year) ? "2L" : "2";
    }
    const maxDays = DateUtilityService.noOfDaysInMonthMap[monthKey];

    if (maxDays == null)
      return false;
    if (date.day > maxDays) {
      return false;
    }

    return true;
  }

  /**
   * Parse the input string of date and convert it into custom Date object.
   * @param date - string
   */
  parseDate(date: string) {
    const splits = date.split("-");

    if (splits == null || splits.length != 3) {
      throw new Error("Invalid Input: " + date);
    }

    const day = parseInt(splits[0]);
    const month = parseInt(splits[1]);
    const year = parseInt(splits[2]);

    if (!this.isAValidDate({ day, month, year })) {
      throw new Error("Not a valid date input: " + day
        + "/" + month + "/" + year + " Or keep the date in range 1900 to 2010");
    }

    return new DateType(day, month, year);
  }
  //compare the input date and find the days difference with repect to with 1-1-1900. 
  compareToOrigin(date: DateType) {
    let difference = 0;
    for (let i = 1900; i < date.year; i++) {
      if (this.isLeapYear(i)) {
        difference += 366;  
      } else
        difference += 365;
    }

    for (let month = 1; month < date.month; month++) {
      let monthKey = date.month + "";
      if (date.month === 2) { // febrary
        monthKey = this.isLeapYear(date.year) ? "2L" : "2";
      }
      difference += DateUtilityService.noOfDaysInMonthMap[monthKey];
    }

    difference += date.day - 1;

    return difference;
  }

/**
 * returns the no of days between 2 dates.
 * @param parsedDate1 
 * @param parsedDate2 
 */
  calcDiffDays(parsedDate1: DateType, parsedDate2: DateType): string {
    const firstCompOrig = this.compareToOrigin(parsedDate1);
    const secCompOrig = this.compareToOrigin(parsedDate2);

    return Math.abs(secCompOrig - firstCompOrig).toString();
  }

  /**
   * check if first date input is greater than second date input.
   * @param firstDate
   * @param secondDate 
   */
  compareTwoDates(firstDate, secondDate) {
    if (new Date(secondDate) < new Date(firstDate)) {
      throw new Error("First date must before second date");
    }
    return true;

  }
}
