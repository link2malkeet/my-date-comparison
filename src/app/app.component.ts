import { Component, ViewChild } from '@angular/core';
import { NgForm } from "@angular/forms";
import { DateType } from './DateTypes';
import { DateUtilityService } from './date-utility.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = "Date Comparison"
  @ViewChild('dateForm') submitForm: NgForm;
  diffDate: string;
  errorMessage: string;
  errorFlag: boolean = false;

  constructor(private dateUtilityService: DateUtilityService) { }

  onSubmit() {
    const firstDate = this.submitForm.value.firstDate;
    const secondDate = this.submitForm.value.secondDate;
    try {
      const parsedDate1 = this.dateUtilityService.parseDate(firstDate);
      const parsedDate2 = this.dateUtilityService.parseDate(secondDate);
      this.dateUtilityService.compareTwoDates(firstDate, secondDate);//to check if first date is lesser than second date
      this.diffDate = this.dateUtilityService.calcDiffDays(parsedDate1, parsedDate2);
      this.errorFlag = false;
    }
    catch (e) {
      console.log(e);
      this.errorMessage = e.message;
      this.diffDate = "";
      this.errorFlag = true;
    }
  }
}
