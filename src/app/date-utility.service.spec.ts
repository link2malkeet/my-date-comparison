import { TestBed, inject } from '@angular/core/testing';

import { DateUtilityService } from './date-utility.service';
import { NgForm, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

describe('DateUtilityService', () => {
  let service: DateUtilityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DateUtilityService],
      imports: [BrowserModule, FormsModule]
    });
  });

  beforeEach(() => {
    service = TestBed.get(DateUtilityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('should check the leap year', () => {
    it('test when a year is not a leap year', () => {
      expect(service.isLeapYear(1999)).toBeFalsy();
    });
    it('test when a year is a leap year', () => {
      expect(service.isLeapYear(2004)).toBeTruthy();
    });
  });

  describe('Valdation of a date', () => {
    it('check if input day is greater than 31 and smaller than 1', () => {
      expect(service.isAValidDate({ day: 32, month: 11, year: 2000 })).toBeFalsy();
      expect(service.isAValidDate({ day: -1, month: 11, year: 2000 })).toBeFalsy();

    });
    it('check if input month is greater than 12 and smaller than 1', () => {
      expect(service.isAValidDate({ day: 30, month: 13, year: 2000 })).toBeFalsy();
      expect(service.isAValidDate({ day: 1, month: -1, year: 2000 })).toBeFalsy();

    });
    it('check if input year is greater than 2010 and smaller than 1900', () => {
      expect(service.isAValidDate({ day: 12, month: 11, year: 2011 })).toBeFalsy();
      expect(service.isAValidDate({ day: 1, month: 11, year: 1899 })).toBeFalsy();

    });
  });

  describe('parse a date', () => {

    it('should return Date instance', () => {
      let parsedDate = service.parseDate("21-01-1999");
      expect(parsedDate).toBeDefined();
      expect(parsedDate.day).toEqual(21);
      expect(parsedDate.month).toEqual(1);
      expect(parsedDate.year).toEqual(1999);
    });

    it('should return error on parsing an invalid date input', () => {
      let date = "21-01"
      try {
        let parsedDate = service.parseDate(date);
      } catch (e) { // this is not a ideal way - this could be improved further.
        expect(e.message).toBe("Invalid Input: " + date);

      }
      //let date = "21-01"
      //expect(service.parseDate(date)).toThrowError("Invalid Input: " + date);
    });


  });

  describe('should return date difference with respect to 1-1-1900', () => {
    it('find the days difference with leap year', () => {
      let diff = service.compareToOrigin({ day: 1, month: 11, year: 2000 });
      expect(diff).toEqual(36824);
    });

    it('find the days difference without leap year', () => {
      let diff = service.compareToOrigin({ day: 1, month: 11, year: 2001 });
      expect(diff).toEqual(37190);
    });

  });

  describe('should return date difference between two dates', () => {
    it('find date diff', () => {
      expect(service.calcDiffDays({ day: 23, month: 10, year: 2000 },
        { day: 23, month: 9, year: 2001 })).toBe("327");
    });


  });

  describe('should check if second date is greater than first date', () => {
    it('should return false if first date is greater than second date', () => {
      try {
        service.compareTwoDates("3-10-1998",
          "03-10-1997");
      } catch (e) { // this is not a ideal way - this could be improved further.
        expect(e.message).toBe("First date must before second date");
      }
    });
  });
});
